package ru.t1.stroilov.tm.component;

import ru.t1.stroilov.tm.api.component.IBootstrap;
import ru.t1.stroilov.tm.api.controller.ICommandController;
import ru.t1.stroilov.tm.api.controller.IProjectController;
import ru.t1.stroilov.tm.api.controller.ITaskController;
import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.service.ICommandService;
import ru.t1.stroilov.tm.api.service.IProjectService;
import ru.t1.stroilov.tm.api.service.ITaskService;
import ru.t1.stroilov.tm.constant.AppConstant;
import ru.t1.stroilov.tm.constant.ArgumentConstant;
import ru.t1.stroilov.tm.controller.CommandController;
import ru.t1.stroilov.tm.controller.ProjectController;
import ru.t1.stroilov.tm.controller.TaskController;
import ru.t1.stroilov.tm.repository.CommandRepository;
import ru.t1.stroilov.tm.repository.ProjectRepository;
import ru.t1.stroilov.tm.repository.TaskRepository;
import ru.t1.stroilov.tm.service.CommandService;
import ru.t1.stroilov.tm.service.ProjectService;
import ru.t1.stroilov.tm.service.TaskService;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(String... args) {
        commandController.showWelcome();
        runApplication(args);
        runInput();
    }

    private void runInput() {
        System.out.println("Please enter command: ");
        while (!Thread.currentThread().isInterrupted()) {
            parseInputArgument(TerminalUtil.nextLine());
            System.out.println();
        }
    }

    private void runApplication(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showUnknownCommand(arg);
                break;
        }
        shutDownApplication();
    }

    private void parseInputArgument(final String input) {
        if (input == null || input.isEmpty()) return;
        switch (input) {
            case AppConstant.VERSION:
                commandController.showVersion();
                break;
            case AppConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case AppConstant.HELP:
                commandController.showHelp();
                break;
            case AppConstant.COMMANDS:
                commandController.showCommands();
                break;
            case AppConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case AppConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case AppConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case AppConstant.PROJECT_DELETE:
                projectController.clearProjects();
                break;
            case AppConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectByID();
                break;
            case AppConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case AppConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectByID();
                break;
            case AppConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case AppConstant.PROJECT_DELETE_BY_ID:
                projectController.removeProjectByID();
                break;
            case AppConstant.PROJECT_DELETE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case AppConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case AppConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case AppConstant.TASK_DELETE:
                taskController.clearTasks();
                break;
            case AppConstant.TASK_SHOW_BY_ID:
                taskController.showTaskByID();
                break;
            case AppConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case AppConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskByID();
                break;
            case AppConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case AppConstant.TASK_DELETE_BY_ID:
                taskController.removeTaskByID();
                break;
            case AppConstant.TASK_DELETE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case AppConstant.EXIT:
                shutDownApplication();
                break;
            default:
                commandController.showUnknownCommand(input);
                break;
        }
    }

    private void shutDownApplication() {
        System.exit(0);
    }

}