package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public TaskRepository() {
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

    @Override
    public Task findByID(String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task delete(Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task deleteByID(String id) {
        final Task task = findByID(id);
        if (task == null) return null;
        return delete(task);
    }

    @Override
    public Task deleteByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        return delete(task);
    }
}
