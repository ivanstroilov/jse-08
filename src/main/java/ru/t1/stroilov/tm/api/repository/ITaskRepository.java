package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    void deleteAll();

    Task findByID(String id);

    Task findByIndex(Integer index);

    Task delete(Task task);

    Task deleteByID(String id);

    Task deleteByIndex(Integer index);

}
