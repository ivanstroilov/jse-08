package ru.t1.stroilov.tm.api.service;

import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task updateByID(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
